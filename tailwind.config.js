module.exports = {
  purge: ["./src/**/*.{js,jsx,ts,tsx}", "./public/index.html"],
  darkMode: false, // or 'media' or 'class'
  corePlugins: {
    container: false,
  },
  theme: {
    extend: {
      minWidth: {},
      fontFamily: {
        // sans: ["Roboto"], // default font
        palatino: ["Palatino"],
      }
    },
  },
  plugins: [],
}
